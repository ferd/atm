#/usr/bin/env python
# -*- coding: utf-8 -*-

class AtmError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value



class Atm:
    """A Python ATM"""
    def __init__(self, amount_refill):
        self.amount = amount_refill
        self.accounts = {}
        self.current_account = None

    def account(self, name, amount):
        self.accounts[name] = amount
        return True

    def insert_card(self, name):
        self.current_account = name
        return self.accounts[name]

    def remove_card(self):
        self.current_account = None
        return True

    def withdraw(self, amount):
        if amount > self.accounts[self.current_account]:
            raise AtmError("overdraft forbidden")
        elif amount > self.amount:
            raise AtmError("ATM overdraft forbidden")
        self.accounts[self.current_account] -= amount
        self.amount -= amount
        return self.accounts[self.current_account]

    def deposit(self, amount):
        self.accounts[self.current_account] += amount
        self.amount += amount
        return self.accounts[self.current_account]

# Test Interface...

# Convert all exceptions to values. For this to make sense, all results are
# wrapped in a tuple ("ok", value) or ("error", str(exception)).
def to_val(f):
    def catcher(*args):
        try:
            return ("ok", f(*args))
        except Exception as e:
            return ("error", str(e))

    return catcher

# Gotta use the global scope because I have no idea how to
# keep state active with erlport otherwise
atm = Atm(1000)

set         = to_val(lambda key, val: setattr(atm, key, val))
account     = to_val(lambda name, amount: atm.account(name, amount))
withdraw    = to_val(lambda amount: atm.withdraw(amount))
deposit     = to_val(lambda amount: atm.deposit(amount))
insert_card = to_val(lambda name: atm.insert_card(name))
remove_card = to_val(lambda: atm.remove_card())
