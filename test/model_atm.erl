-module(model_atm).
-include_lib("proper/include/proper.hrl").
-export([total/0, accounts/0]).
-export([command/1, initial_state/0, next_state/3,
         precondition/2, postcondition/3]).

-record(state, {user,
                atm_amount,
                accounts}).

total() -> 200.

accounts() ->
    [{<<"bob">>, 100},
     {<<"julie">>, 100},
     {<<"mark">>, 50}].

account() ->
    oneof([A || {A,_} <- accounts()]).

command(_State) ->
    oneof([
        {call, shim, insert_card, [account()]},
        {call, shim, remove_card, []},
        {call, shim, withdraw, [pos_integer()]},
        {call, shim, deposit, [pos_integer()]}
    ]).

%% Initial model value at system start. Should be deterministic.
initial_state() ->
    #state{atm_amount = total(),
           accounts = maps:from_list(accounts())}.

%% Picks whether a command should be valid under the current state.

%% Only allow operations when users are logged in
precondition(#state{user=undefined}, {call, _, withdraw, _}) ->
    false;
precondition(#state{user=undefined}, {call, _, deposit, _}) ->
    false;
precondition(#state{user=User}, {call, _, remove_card, _}) ->
    User =/= undefined;
precondition(#state{user=User}, {call, _, insert_card, _}) ->
    User =:= undefined;
precondition(#state{}, {call, _Mod, _Fun, _Args}) ->
    true.

%% Given the state `State' *prior* to the call `{call, Mod, Fun, Args}',
%% determine whether the result `Res' (coming from the actual system)
%% makes sense.
postcondition(#state{accounts=Accs}, {call, _, insert_card, [User]}, Res) ->
    %% When logging on, the account balance is returned and matches
    %% our expectations
    #{User := Balance} = Accs,
    Res =:= {ok, Balance};
postcondition(#state{}, {call, _, remove_card, []}, Res) ->
    Res =:= {ok, true};
postcondition(#state{user=User, accounts=Accs},
              {call, _, deposit, [Amount]}, Res) ->
    %% The ATM returns the proper amount of money in the account
    %% following a deposit.
    #{User := Balance} = Accs,
    Res =:= {ok, Amount+Balance};
postcondition(#state{user=User, accounts=Accs, atm_amount=AtmAmount},
              {call, _, withdraw, [Amount]}, Res) ->
    %% Rules here
    %% - the amount matches what the model tracks
    %% - exceptions if the amount is greater than what the account
    %%   balance allows
    #{User := Balance} = Accs,
    if Balance < Amount ->
         Res =:= {error, <<"overdraft forbidden">>}
     ; AtmAmount < Amount ->
         Res =:= {error, <<"ATM overdraft forbidden">>}
     ; Balance >= Amount, AtmAmount >= Amount ->
         Res =:= {ok, Balance - Amount}
    end;
postcondition(_State, {call, _Mod, _Fun, _Args}, _Res) ->
    true.

%% Assuming the postcondition for a call was true, update the model
%% accordingly for the test to proceed.
%%
%% `next_state' is called with both symbolic (`{var, X}', `{call, ...}')
%% values and the literal values, depending on when it runs when generating
%% commands, or after postconditions are evaluated; both will happen.
next_state(S=#state{}, _Res, {call, _, insert_card, [User]}) ->
    S#state{user=User};
next_state(S=#state{}, _Res, {call, _, remove_card, []}) ->
    S#state{user=undefined};
next_state(S=#state{user=User, accounts=Accs, atm_amount=AtmAmount}, _Res,
           {call, _, deposit, [Amount]}) ->
    %% the deposit worked so we track the new balance
    #{User := UserAmount} = Accs,
    S#state{accounts=Accs#{User => UserAmount+Amount},
            atm_amount=AtmAmount+Amount};
next_state(S=#state{user=User, accounts=Accs, atm_amount=AtmAmount}, _Res,
           {call, _, withdraw, [Amount]}) ->
    %% if the withdrawal worked we track the new balance
    #{User := UserAmount} = Accs,
    case UserAmount >= Amount andalso AtmAmount >= Amount of
        true -> % withdrawal succeeded
            S#state{accounts=Accs#{User => UserAmount-Amount},
                    atm_amount=AtmAmount-Amount};
        false -> % withdrawal failed
            S
    end;
next_state(State, _Res, {call, _Mod, _Fun, _Args}) ->
    NewState = State,
    NewState.

