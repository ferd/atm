-module(prop_atm).
-include_lib("proper/include/proper.hrl").
-define(MODEL, model_atm).

prop_test() ->
    ?FORALL(Cmds, commands(?MODEL),
            begin
                shim:start(?MODEL:total(), ?MODEL:accounts()),
                {History, State, Result} = run_commands(?MODEL, Cmds),
                shim:stop(),
                ?WHENFAIL(io:format("History: ~p\nState: ~p\nResult: ~p\n",
                                    [History,State,Result]),
                          aggregate(command_names(Cmds), Result =:= ok))
            end).

seq_len(Cmds) ->
    [length(Cmds)].

accounts(Cmds) ->
    [lists:usort(
        [binary_to_atom(Name, utf8) || {set, _, {call, _, insert_card, [Name]}} <- Cmds]
    )].
