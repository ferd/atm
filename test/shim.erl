-module(shim).
-behaviour(gen_server).
-export([start/2, stop/0, total/0, insert_card/1, remove_card/0,
         withdraw/1, deposit/1, recharge/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, code_change/3, terminate/2]).

start(Total, Accounts) ->
    {ok, Pid} = gen_server:start({local, ?MODULE}, ?MODULE, [], []),
    call(set, [<<"amount">>, Total]),
    [call(account, [Account, Amount]) || {Account, Amount} <- Accounts],
    {ok, Pid}.

stop() ->
    gen_server:stop(?MODULE, normal, timer:seconds(5)).

total() ->
    call(total, []).

insert_card(Name) ->
    call(insert_card, [Name]).

remove_card() ->
    call(remove_card, []).

withdraw(N) ->
    call(withdraw, [N]).

deposit(N) ->
    call(deposit, [N]).

recharge() ->
    call(recharge, []).

%%%%%%%%%%%%%%%%%%
%%% GEN_SERVER %%%
%%%%%%%%%%%%%%%%%%
%% holds the python connection alive
call(F, Args) ->
    case gen_server:call(?MODULE, {call, F, Args}) of
        {<<"ok">>, Val} -> {ok, Val};
        {<<"error">>, Val} -> {error, Val}
    end.

init([]) ->
    python:start([{python_path, "./priv/"}]).

handle_call({call, F, Args}, _From, Python) ->
    {reply, python:call(Python, atm, F, Args), Python}.

handle_cast(_, Python) ->
    {noreply, Python}.

handle_info(_, Python) ->
    {noreply, Python}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

terminate(_, Python) ->
    python:stop(Python).

